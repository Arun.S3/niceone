package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class TC00PackageMethod extends SeMethods {
	
@Parameters({"url","username","password"})

	@BeforeMethod(groups= {"any"})
	public void login(String url, String uname, String Pass) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("password");
		type(elePassword, Pass);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement clickcrmsfa = locateElement("linkedText", "CRM/SFA");
		click(clickcrmsfa);
	}

	@AfterMethod(groups= {"any"})
	public void closebrowser() {

		closeBrowser();
	}
}

package testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class Zoomcar extends SeMethods {
@Test
	public void Zoom() throws InterruptedException {
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement start = locateElement("linkedText", "Start your wonderful journey");
		click(start);
		WebElement populoc = locateElement("xpath", "//div[@class='component-popular-locations']/div[4]");
		click(populoc);
		WebElement next = locateElement("xpath", "//button[@class='proceed']");
		click(next);
		
		int date = nextDate();
		WebElement clickdate = locateElement("xpath", "//div[contains(text(),'"+date+"')]");
		click(clickdate);
		
	WebElement nxt = locateElement("class", "proceed");
	click(nxt);
	
	WebElement done = locateElement("class", "proceed");
	click(done);
	Thread.sleep(2000);
	List<WebElement> priceval =locateElements("//div[@class='price']");
	
	List <Integer> val1 = new ArrayList<>();
	
	int size = priceval.size();
	System.out.println(size);
	for (WebElement eachprice : priceval) {
		String text = eachprice.getText();
		int len = text.length();
		String substring = text.substring(2, len);
		System.out.println(substring);
		int val = Integer.parseInt(substring);
		val1.add(val);
		//System.out.println("maximum amount ="+val1);
	}
		Integer max = Collections.max(val1);
		System.out.println("Maximum value is:" +max);
		
		

	}
}

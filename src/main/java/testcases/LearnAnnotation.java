package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class LearnAnnotation {

	@BeforeSuite
	public void beforeSuite() {
		
		System.out.println("@beforeSuite");
	}
	
	@BeforeTest
	 	public void beforeTest() {
		System.out.println("@beforeTest");
	}
	@BeforeClass
	
		public void beforeClass() {
		System.out.println("@beforeTest");
	}
	/*@BeforeMethod
	@Test
	
	@AfterMethod
	@AfterClass
	@AfterTest
	@AfterSuite*/
	
	
}

package testcases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import wdMethods.TC00PackageMethod;


public class TC002EditLead extends TC00PackageMethod {
	@Test(groups= {"sanity"})
	public void editLead() throws InterruptedException {

	
	WebElement eleclicklead = locateElement("linkedText", "Leads");
	click(eleclicklead);
	
	WebElement eleclickCreateLead = locateElement("linkedText", "Create Lead");
	click(eleclickCreateLead);
	
	WebElement compname = locateElement("class", "inputBox");
	type(compname, "SCB");
	
	WebElement enterFname = locateElement("id", "createLeadForm_firstName");
	type(enterFname, "MANOJ");
	
	WebElement enterLname = locateElement("id", "createLeadForm_lastName");
	type(enterLname, "KIYAN");
	
	WebElement Salutation = locateElement("id", "createLeadForm_personalTitle");
	type(Salutation, "MR");
	
	WebElement crtleadclick = locateElement("class", "smallSubmit");
	click(crtleadclick);
	
	Thread.sleep(3000);
	
	WebElement editbutton = locateElement("linkedText", "Edit");
	click(editbutton);
	
	WebElement clrcomp = locateElement("id", "updateLeadForm_firstName");
	clear(clrcomp);
	type(clrcomp, "CAPGEMINI");
	
	WebElement updateBtn = locateElement("class", "smallSubmit");
	click(updateBtn);
	
	
	}
}

package testcases;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import wdMethods.TC00PackageMethod;

public class TC001CreateLead extends TC00PackageMethod {

	@Test(dataProvider="dp")
	public void createLead(String canme, String fname, String last, String stat) {

		WebElement eleclicklead = locateElement("linkedText", "Leads");
		click(eleclicklead);

		WebElement eleclickCreateLead = locateElement("linkedText", "Create Lead");
		click(eleclickCreateLead);

		WebElement compname = locateElement("class", "inputBox");
		type(compname, canme);

		WebElement enterFname = locateElement("id", "createLeadForm_firstName");
		type(enterFname, fname);

		WebElement enterLname = locateElement("id", "createLeadForm_lastName");
		type(enterLname, last);

		WebElement Salutation = locateElement("id", "createLeadForm_personalTitle");
		type(Salutation, stat);

		WebElement crtleadclick = locateElement("class", "smallSubmit");
		click(crtleadclick);

	}

	@DataProvider(name = "dp", indices = {1,2})
	public Object [][] fetchData() throws IOException {
		Object[][] data = ExcelRead.createLead();
		return data;
		/*String[][] data = new String[2][4];
		data[0][0] = "capgemini";
		data[0][1] = "manoj";
		data[0][2] = "j";
		data[0][3] = "Mr";
		
		data[1][0] = "scb";
		data[1][1] = "arun";
		data[1][2] = "s";
		data[1][3] = "Mr";
*/
		//return data;

	}

}

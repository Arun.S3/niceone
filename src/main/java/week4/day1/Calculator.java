package week4.day1;

import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
	
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the 1st number:");
		double var1 = sc.nextDouble();
		
		System.out.println("Enter the 2nd number:");
		double var2 = sc.nextDouble();
		
		System.out.println("Enter the Operation: ADD,MINUS,MULTIPLY,DIVIDE");
		String operation = sc.next();
		
		switch(operation)
		{
		case "ADD":
			System.out.println("Addition of" +var1+ "and" +var2+ "is" +(var1+var2));
			break;
			
		case "MINUS":
			System.out.println("Minus of" +var1+ "and" +var2+ "is" +(var1-var2));
			break;
			
		case "MULTIPLY":
			System.out.println("Multiply of" +var1+ "and" +var2+ "is" +(var1*var2));
			break;
			
		case "DIVIDE":
			if(var2!=0) {
				System.out.println("Division of" +var1+ "and" +var2+ "is" +(var1/var2));
			}
			else
			{
				System.out.println("The divisor cannot be zero");
			}
			
			break;
			
		default:
			System.out.println("Enter a Valid Operation");
		}
			
		}
		
	}

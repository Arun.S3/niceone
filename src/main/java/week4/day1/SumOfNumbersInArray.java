package week4.day1;

public class SumOfNumbersInArray {

	public static void main(String[] args) {
		int[] numb = { 25, 26, 9, 18, 3, 12 };
		int sum = 0;

		for (int i = 0; i < numb.length; i++) {
			sum = sum + numb[i];
		}
		System.out.println(sum);
	}

}

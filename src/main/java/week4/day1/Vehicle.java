package week4.day1;

public class Vehicle {
	//creating constructor- Class name and Method name (same) and constructor dont have return type as Void
	public Vehicle()
	{
		System.out.println("Drive");
	}

	//creating a normal method so it has void function
	public void vehicleType()
	{
		System.out.println("CARS");
	}
}


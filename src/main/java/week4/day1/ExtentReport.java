package week4.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {
	@Test
	public void learnReport() throws IOException {
		
	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
	extent.attachReporter(html);
	
	ExtentTest logger = extent.createTest("TC001CreateLead","Create a new lead");
	logger.assignAuthor("Arun");
	logger.assignCategory("Smoke");
	logger.log(Status.PASS, "xyz", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	
	extent.flush();
	
	

	
	
	}

}
